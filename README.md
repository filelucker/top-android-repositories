
A simple Android application which shows the most starred github repositories by searching with the keyword "Android".

## Used Tech-stack
* Tech-stack
  * Principles - OOP(Object Oriented Programming), SOLID.
  * Kotlin - A cross-platform, statically typed, general-purpose programming language with type inference.
  * MVVM - is the industry-recognized software architecture pattern.
  * Repository Pattern - a repository to abstract the data layer, and integrate the repository class with the ViewModel.
  * Coroutines - concurrency design pattern that you can use on Android to simplify code that executes asynchronously.
  * Sealed class - restricted class hierarchies that provide more control over inheritance.
  * Retrofit - an easy and fast library to retrieve and upload data via a REST-based web service.
  * Room - an ORM ( Object Relational Mapper ) for SQLite database in Android.
  * LiveData - a data holder class that can be observed.
  * ViewModel - manage your UI's data in a lifecycle-aware fashion.
  * Unit tests - verify a very small portion of the app, such as a method or class. (Used only one class)
  * Product flavors - represent different versions of your project that you expect to co-exist on a single device, the Google Play store, or repository.


## Add Screen Shot

<p align="center">
  <a style="text-decoration:none" area-label="home page">
   <img src="https://gitlab.com/filelucker/top-android-repositories/-/raw/master/Screenshots/Screenshot_1687346116.png" width="150" height="280" />
  </a>
  <a style="text-decoration:none" area-label="detail page">
    <img src="https://gitlab.com/filelucker/top-android-repositories/-/raw/master/Screenshots/Screenshot_1687346784.png" width="150" height="280" />
  </a>
</p>
