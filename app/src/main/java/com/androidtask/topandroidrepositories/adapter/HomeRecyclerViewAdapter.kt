package com.androidtask.topandroidrepositories.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.androidtask.topandroidrepositories.R
import com.androidtask.topandroidrepositories.databinding.LayoutRecyclerHomeItemBinding
import com.androidtask.topandroidrepositories.model.RepositoryItemModel
import com.bumptech.glide.Glide

class HomeRecyclerViewAdapter(
    private val context: Context,
    private var modelDataList: ArrayList<RepositoryItemModel>
) :
    RecyclerView.Adapter<HomeRecyclerViewAdapter.ViewHolder>() {
    private lateinit var binding: LayoutRecyclerHomeItemBinding
    private lateinit var onItemClickListener: SetOnItemClickListener

    internal fun setDataList(modelDataList: ArrayList<RepositoryItemModel>): HomeRecyclerViewAdapter {
        this.modelDataList = modelDataList
        notifyDataSetChanged()
        return this
    }

    internal fun setOnClickListener(onItemClickListener: SetOnItemClickListener): HomeRecyclerViewAdapter {
        this.onItemClickListener = onItemClickListener
        return this
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun getItemCount(): Int {
        return modelDataList.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        binding = LayoutRecyclerHomeItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HomeRecyclerViewAdapter.ViewHolder, position: Int) {
        holder.apply {
            setIsRecyclable(false)
            bind(modelDataList[position], position)
        }
    }

    inner class ViewHolder(itemView: LayoutRecyclerHomeItemBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        fun bind(item: RepositoryItemModel, position: Int) {
            binding.apply {
                idTitle.text = item.fullName
                idAuthor.text = "@${item.owner.login}"
                idDescription.text = item.description
                Glide.with(context).load(item.owner.avatarUrl)
                    .placeholder(R.drawable.img_avatar_default)
                    .into(idUserImage)
                onItemClickListener.let { onItemClick ->
                    itemView.setOnClickListener {
                        onItemClick.setOItemClickListener(it, item, position)
                    }
                }
            }
        }
    }

    interface SetOnItemClickListener {
        fun setOItemClickListener(view: View, itemData: RepositoryItemModel, position: Int)
    }
}