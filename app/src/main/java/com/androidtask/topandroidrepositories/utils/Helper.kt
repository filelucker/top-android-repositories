package com.androidtask.topandroidrepositories.utils

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.ActionBar
import java.io.Serializable
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date

class Helper {
    companion object {
        @SuppressLint("SimpleDateFormat")
        fun dateFormatter(
            date: String,
            currentFormat: String,
            expectedFormat: String = "yyyy-MM-dd"
        ): String {
            val dateFormat: DateFormat = SimpleDateFormat(currentFormat)
            val formattedDate: Date = dateFormat.parse(date) as Date
            val formatter: DateFormat = SimpleDateFormat(expectedFormat)
            return formatter.format(formattedDate)
        }

        @SuppressLint("SimpleDateFormat")
        fun millisToDate(milliSecond: Long, dateFormat: String = "yyyy-MM-dd HH:mm:ss"): String {
            val formatter: SimpleDateFormat = SimpleDateFormat(dateFormat)
            return formatter.format(Date(milliSecond))
        }

        fun setActionBar(
            actionBar: ActionBar,
            barTitle: String,
            displayHomeButton: Boolean = false
        ) {
            actionBar.let { itActonBar ->
                itActonBar.apply {
                    title = barTitle
                    setDisplayHomeAsUpEnabled(displayHomeButton)
                }
            }
        }
    }
}

inline fun <reified T : Serializable> Bundle.serializable(key: String): T? = when {
    Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU -> getSerializable(key, T::class.java)
    else -> @Suppress("DEPRECATION") getSerializable(key) as? T
}