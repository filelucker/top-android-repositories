package com.androidtask.topandroidrepositories.utils

object AppConstants {
    const val perPage: Int = 10
    private const val jsonPlaceHolder: String = "https://jsonplaceholder.typicode.com"
    private const val gitHubApi: String = "https://api.github.com"
    const val baseUrl = gitHubApi
    const val getRepositories: String = "/search/repositories"
    const val bundleKeyDetails: String = "details_data_model"
    const val responseTimeOut: Long = 30 * 60 * 1000
}