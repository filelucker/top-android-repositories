package com.androidtask.topandroidrepositories.model

import com.google.gson.annotations.SerializedName

data class PostModel(val body: String)
