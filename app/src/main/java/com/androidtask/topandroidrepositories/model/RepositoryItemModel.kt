package com.androidtask.topandroidrepositories.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.androidtask.topandroidrepositories.gsonconverter.RepositoryOwnerConverter
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "tbl_repository_item")
data class RepositoryItemModel(
    @PrimaryKey(autoGenerate = false)  @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("full_name") val fullName: String,
    @ColumnInfo(name = "owner")
    @TypeConverters(RepositoryOwnerConverter::class)
    @SerializedName("owner") val owner: RepositoryOwnerModel,
    @SerializedName("html_url") val htmlUrl: String,
    @SerializedName("description") val description: String,
    @SerializedName("watchers_count") val watchersCount: Int,
    @SerializedName("forks_count") val forksCount: Int,
    @SerializedName("updated_at") val updatedAt: String
) : Serializable
