package com.androidtask.topandroidrepositories.model

data class RepositoryWithOwnerModel(
    val repositoryModel: RepositoryModel
)
