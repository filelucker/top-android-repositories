package com.androidtask.topandroidrepositories.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.androidtask.topandroidrepositories.gsonconverter.RepositoryItemConverter
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "tbl_search_response")
data class SearchResponseModel(
    @PrimaryKey(autoGenerate = false) @SerializedName("id") var id: Int,
    @ColumnInfo(name = "total_count") @SerializedName("total_count") val totalCount: Int,
    @ColumnInfo(name = "incomplete_results") @SerializedName("incomplete_results") val incompleteResults: Boolean,
    @SerializedName("page") var page: Int = 1,
    @ColumnInfo(name = "data_type") @SerializedName("data_type") var dataType: String = "live_data",
    @ColumnInfo(name = "time_in_millis") @SerializedName("time_in_millis") var currentTimeMillis: String,
    @ColumnInfo(name = "items")
    @TypeConverters(RepositoryItemConverter::class)
    @SerializedName("items") val itemList: ArrayList<RepositoryItemModel>
) : Serializable

