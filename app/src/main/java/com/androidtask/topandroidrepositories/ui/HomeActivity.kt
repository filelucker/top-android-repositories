package com.androidtask.topandroidrepositories.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidtask.topandroidrepositories.adapter.HomeRecyclerViewAdapter
import com.androidtask.topandroidrepositories.databinding.ActivityHomeBinding
import com.androidtask.topandroidrepositories.factory.ViewModelFactory
import com.androidtask.topandroidrepositories.model.RepositoryItemModel
import com.androidtask.topandroidrepositories.model.SearchResponseModel
import com.androidtask.topandroidrepositories.network.ResponseData
import com.androidtask.topandroidrepositories.network.RetrofitApi
import com.androidtask.topandroidrepositories.network.RetrofitBuilder
import com.androidtask.topandroidrepositories.repository.SearchRepository
import com.androidtask.topandroidrepositories.utils.AppConstants
import com.androidtask.topandroidrepositories.viewmodel.SearchViewModel
import java.io.Serializable

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private lateinit var viewModel: SearchViewModel

    private lateinit var recyclerViewAdapter: HomeRecyclerViewAdapter
    private var adapterDataList = ArrayList<RepositoryItemModel>()
    private lateinit var searchResponseModel: SearchResponseModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()
        //setupSampleViewModel()
        setupViewModel()
    }

    private fun setupRecyclerView() {
        recyclerViewAdapter = HomeRecyclerViewAdapter(this, ArrayList())
            .setOnClickListener(object : HomeRecyclerViewAdapter.SetOnItemClickListener {
                override fun setOItemClickListener(
                    view: View,
                    itemData: RepositoryItemModel,
                    position: Int
                ) {
                    val intent = Intent(this@HomeActivity, DetailsActivity::class.java)
                    intent.let { letIntent ->
                        letIntent.apply {
                            val bundle = Bundle()
                            bundle.putSerializable(
                                AppConstants.bundleKeyDetails,
                                itemData as Serializable
                            )
                            putExtras(bundle)
                            startActivity(this)
                        }
                    }
                }

            })

        binding.idRecyclerViewHome.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = recyclerViewAdapter
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (!canScrollVertically(1) && adapterDataList.size < searchResponseModel.totalCount) {
                        //println("DEBUG_LOG_PRINT: onScrolled can not scroll vertically")
                        viewModel.getRepository()
                    }

                }

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    println("DEBUG_LOG_PRINT: onScrollStateChanged")
                }
            })
        }
    }

    private fun setupViewModel() {
        val retrofitBuilder = RetrofitBuilder().buildApi(RetrofitApi::class.java)
        val factory = ViewModelFactory(SearchRepository(this, retrofitBuilder))
        viewModel = ViewModelProvider(this, factory)[SearchViewModel::class.java]
        //
        viewModel.getRepository()
        viewModel.response.observe(this, Observer {
            when (it) {
                is ResponseData.Success -> {
                    searchResponseModel = it.data as SearchResponseModel
                    //adapterDataList = dataModel.itemList
                    adapterDataList.addAll(searchResponseModel.itemList)
                    recyclerViewAdapter.setDataList(adapterDataList)
                    //println("DEBUG_LOG_PRINT: viewModel observe Success statement ${searchResponseModel.toString()}")
                    println("DEBUG_LOG_PRINT: viewModel observe Success statement ${searchResponseModel.toString()}")
                }

                is ResponseData.Failed -> {
                    println("DEBUG_LOG_PRINT: viewModel observe Failed statement")
                    println("DEBUG_LOG_PRINT: ${it.data.toString()}")
                }

                else -> {
                    println("DEBUG_LOG_PRINT: viewModel observe else statement")
                }
            }
        })
    }

}