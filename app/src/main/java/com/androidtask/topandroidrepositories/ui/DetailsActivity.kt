package com.androidtask.topandroidrepositories.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.androidtask.topandroidrepositories.R
import com.androidtask.topandroidrepositories.databinding.ActivityDetailsBinding
import com.androidtask.topandroidrepositories.model.RepositoryItemModel
import com.androidtask.topandroidrepositories.utils.AppConstants
import com.androidtask.topandroidrepositories.utils.Helper
import com.androidtask.topandroidrepositories.utils.serializable
import com.bumptech.glide.Glide

class DetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailsBinding
    private lateinit var repositoryItemModel: RepositoryItemModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val bundle: Bundle? = intent.extras
        bundle?.let { itBundle ->
            itBundle.serializable<RepositoryItemModel>(AppConstants.bundleKeyDetails).let {
                repositoryItemModel = it!!
            }
        }

        setDetailsView()
    }

    private fun setDetailsView() {
        repositoryItemModel.let { itRepoItemModel ->
            binding.apply {
                idAuthor.text = "@${itRepoItemModel.owner.login}"
                idLastUpdate.text = Helper.dateFormatter(
                    itRepoItemModel.updatedAt,
                    "yyyy-MM-dd'T'HH:mm:ss",
                    "MM-dd-yyyy HH:mm:ss"
                )
                idDescription.text = itRepoItemModel.description
                Glide.with(this@DetailsActivity).load(itRepoItemModel.owner.avatarUrl)
                    .into(idUserImage)
            }
        }
    }
}