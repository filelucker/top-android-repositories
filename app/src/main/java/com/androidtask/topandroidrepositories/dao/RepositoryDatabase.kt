package com.androidtask.topandroidrepositories.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.androidtask.topandroidrepositories.gsonconverter.RepositoryItemConverter
import com.androidtask.topandroidrepositories.gsonconverter.RepositoryOwnerConverter
import com.androidtask.topandroidrepositories.model.SearchResponseModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

@Database(entities = [SearchResponseModel::class], version = 1)
@TypeConverters(RepositoryItemConverter::class, RepositoryOwnerConverter::class)
abstract class RepositoryDatabase : RoomDatabase() {
    abstract fun repositoryDao(): RepositoryDao

    companion object {
        private var instance: RepositoryDatabase? = null

        @Synchronized
        fun getInstance(context: Context): RepositoryDatabase {
            if (instance == null) {
                synchronized(RepositoryDatabase::class.java) {
                    instance = Room.databaseBuilder(
                        context.applicationContext, RepositoryDatabase::class.java,
                        "github_repository_search"
                    )
                        .fallbackToDestructiveMigration()
                        //.addCallback(roomCallback)
                        .build()
                }
            }

            return instance!!

        }


    }
}