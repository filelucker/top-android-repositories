package com.androidtask.topandroidrepositories.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.androidtask.topandroidrepositories.model.RepositoryItemModel
import com.androidtask.topandroidrepositories.model.SearchResponseModel

@Dao
interface RepositoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(searchResponseModel: SearchResponseModel)

    @Update
    fun update(searchResponseModel: SearchResponseModel)

    @Delete
    fun delete(searchResponseModel: SearchResponseModel)

    @Query("DELETE FROM tbl_search_response")
    fun deleteAllSearchRepo()

    @Query("DELETE FROM tbl_search_response WHERE time_in_millis <= :date")
    fun deleteSearchRepoByDate(date: String)

    @Query("SELECT * FROM tbl_search_response")
    fun getAllSearchRepo(): SearchResponseModel
    @Query("SELECT * FROM tbl_search_response WHERE page = :page")
    fun getSearchRepoByPage(page: Int): SearchResponseModel
}
