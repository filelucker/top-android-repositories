package com.androidtask.topandroidrepositories.gsonconverter

import androidx.room.TypeConverter
import com.androidtask.topandroidrepositories.model.RepositoryItemModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class RepositoryItemConverter {
    @TypeConverter
    fun toRepoItemList(repoItemJson: String): ArrayList<RepositoryItemModel> {
        val type = object : TypeToken<ArrayList<RepositoryItemModel>>() {}.type
        return Gson().fromJson(repoItemJson, type)
    }

    @TypeConverter
    fun repoItemToJson(repoItemList: ArrayList<RepositoryItemModel>): String {
        val type = object : TypeToken<ArrayList<RepositoryItemModel>>() {}.type
        return Gson().toJson(repoItemList, type)
    }
}