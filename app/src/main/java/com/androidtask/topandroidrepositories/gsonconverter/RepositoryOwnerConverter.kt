package com.androidtask.topandroidrepositories.gsonconverter

import androidx.room.TypeConverter
import com.androidtask.topandroidrepositories.model.RepositoryOwnerModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class RepositoryOwnerConverter {
    @TypeConverter
    fun toRepoOwnerList(repoOwnerJson: String): ArrayList<RepositoryOwnerModel> {
        val type = object : TypeToken<ArrayList<RepositoryOwnerModel>>() {}.type
        return Gson().fromJson(repoOwnerJson, type)
    }

    @TypeConverter
    fun repoOwnerToJson(repoOwnerList: ArrayList<RepositoryOwnerModel>): String {
        val type = object : TypeToken<ArrayList<RepositoryOwnerModel>>() {}.type
        return Gson().toJson(repoOwnerList, type)
    }
}