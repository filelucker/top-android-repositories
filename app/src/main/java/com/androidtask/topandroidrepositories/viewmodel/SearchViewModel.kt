package com.androidtask.topandroidrepositories.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androidtask.topandroidrepositories.model.SearchResponseModel
import com.androidtask.topandroidrepositories.network.ResponseData
import com.androidtask.topandroidrepositories.repository.SearchRepository
import kotlinx.coroutines.launch

class SearchViewModel(private val repository: SearchRepository) : ViewModel() {
    private val _response: MutableLiveData<ResponseData<SearchResponseModel>> = MutableLiveData()
    val response: LiveData<ResponseData<SearchResponseModel>>
        get() = _response
    private var pageNumber: Int = 0

    fun getRepository() = viewModelScope.launch {
        pageNumber += 1
        _response.value = repository.getRepository("android", pageNumber) as ResponseData<SearchResponseModel>
    }
}