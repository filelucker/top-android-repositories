package com.androidtask.topandroidrepositories.network

import com.androidtask.topandroidrepositories.model.PostModel
import com.androidtask.topandroidrepositories.model.RepositoryItemModel
import com.androidtask.topandroidrepositories.model.SearchResponseModel
import com.androidtask.topandroidrepositories.utils.AppConstants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

//|------------------|INTERFACE - RETROFIT API|------------------|
interface RetrofitApi {
    //|---------------|STATIC - COMPANION OBJECT|----------------|
    companion object {
        private const val PER_PAGE = 50
    }

    //|-----------|METHOD - SAMPLE DATA (GET METHOD)|------------|

    @GET("/posts")
    suspend fun sampleData(): Response<ArrayList<PostModel>>

    //|--------|METHOD - SEARCH REPOSITORY (GET METHOD)|---------|
    @GET(AppConstants.getRepositories)
    suspend fun searchRepository(
        @Query("q") q: String,
        @Query("sort") sort: String,
        @Query("order") order: String,
        @Query("per_page") per_page: Int = AppConstants.perPage,
        @Query("page") page: Int
    ): Response<SearchResponseModel>
}