package com.androidtask.topandroidrepositories.network

import com.androidtask.topandroidrepositories.utils.AppConstants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


//|------------------|CLASS - RETROFIT BUILDER|------------------|
class RetrofitBuilder {
    /*companion object {
        private const val BASE_URL = "https://api.github.com"
    }*/

    //|-------------------|METHOD - BUILD API|-------------------|
    fun <Api> buildApi(api: Class<Api>): Api {
        return Retrofit.Builder()
            .baseUrl(AppConstants.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(api)
    }
    private val okHttpClient by lazy {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
        val okHttpClient: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
        okHttpClient
    }
}