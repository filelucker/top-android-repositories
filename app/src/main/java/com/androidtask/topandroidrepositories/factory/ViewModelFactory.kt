package com.androidtask.topandroidrepositories.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.androidtask.topandroidrepositories.repository.BaseRepository
import com.androidtask.topandroidrepositories.repository.SearchRepository
import com.androidtask.topandroidrepositories.viewmodel.SearchViewModel
import java.lang.IllegalArgumentException

class ViewModelFactory(private val repository: BaseRepository): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(SearchViewModel::class.java) -> SearchViewModel(repository as SearchRepository) as T
            else -> throw IllegalArgumentException("ViewModel not found")
        }
    }
}