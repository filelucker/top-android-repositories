package com.androidtask.topandroidrepositories.repository

import com.androidtask.topandroidrepositories.network.ResponseData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

abstract class BaseRepository {
    suspend fun <T> retrofitApi(api: suspend () -> T): ResponseData<T> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.invoke() as Response<T>
                //println("DEBUG_LOG_PRINT: response data ${response.body().toString()}")
                ResponseData.Success(response.body() as T)
            } catch (throwable: Throwable) {
                val message: String = throwable.message!!
                //println("DEBUG_LOG_PRINT: response data message ${message.toString()}")
                ResponseData.Failed(message as T)
            }
        }
    }
}