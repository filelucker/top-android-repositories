package com.androidtask.topandroidrepositories.repository

import android.content.Context
import com.androidtask.topandroidrepositories.dao.RepositoryDao
import com.androidtask.topandroidrepositories.dao.RepositoryDatabase
import com.androidtask.topandroidrepositories.model.SearchResponseModel
import com.androidtask.topandroidrepositories.network.ResponseData
import com.androidtask.topandroidrepositories.network.RetrofitApi
import com.androidtask.topandroidrepositories.utils.AppConstants
import com.androidtask.topandroidrepositories.utils.Helper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SearchRepository(context: Context, private val api: RetrofitApi) : BaseRepository() {
    private lateinit var repositoryDao: RepositoryDao
    private val repositoryDatabase = RepositoryDatabase.getInstance(context)
    private var searchRepositoryModel: SearchResponseModel? = null

    init {
        repositoryDao = repositoryDatabase.repositoryDao()
    }

    suspend fun getRepository(searchQuery: String, pageNumber: Int) = withContext(Dispatchers.IO) {
        deleteSearchRepoByDate()
        searchRepositoryModel = repositoryDao.getSearchRepoByPage(pageNumber)
        searchRepositoryModel?.let {
            /*println("DEBUG_LOG_PRINT: responseModel id ${it.id}")
            println("DEBUG_LOG_PRINT: responseModel data ${it.toString()}")*/
            ResponseData.Success(searchRepositoryModel)
        } ?: run {
            //println("DEBUG_LOG_PRINT: not responseModel")
            callApi(searchQuery, pageNumber)
        }
        //callApi(searchQuery, pageNumber)
    }

    private suspend fun callApi(searchQuery: String, pageNumber: Int) =
        retrofitApi {
            val repositoryData = api.searchRepository(searchQuery, "", "", AppConstants.perPage, pageNumber)
            saveSearchRepository(repositoryData.body() as SearchResponseModel, pageNumber)
            repositoryData
        }

    private suspend fun saveSearchRepository(
        searchRepositoryModel: SearchResponseModel,
        page: Int
    ) {
        withContext(Dispatchers.IO) {
            val currentMillis = System.currentTimeMillis()
            searchRepositoryModel.id = page
            searchRepositoryModel.page = page
            searchRepositoryModel.dataType = "archived_data"
            searchRepositoryModel.currentTimeMillis = Helper.millisToDate(currentMillis)
            repositoryDao.insert(searchRepositoryModel)
        }
    }

    private suspend fun deleteSearchRepoByDate() {
        withContext(Dispatchers.IO) {
            val currentMillis = System.currentTimeMillis() - AppConstants.responseTimeOut
            println("DEBUG_LOG_PRINT: millisToDate ${Helper.millisToDate(currentMillis)}")
            repositoryDao.deleteSearchRepoByDate(Helper.millisToDate(currentMillis))
        }
    }
}