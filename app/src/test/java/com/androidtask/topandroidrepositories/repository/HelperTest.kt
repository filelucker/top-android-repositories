package com.androidtask.topandroidrepositories.repository

import com.androidtask.topandroidrepositories.utils.Helper
import org.junit.Assert.assertEquals
import org.junit.Test

class HelperTest {

    @Test
    fun testDate() {
        val currentDate = "2023-06-21"
        val currentFormat = "yyyy-MM-dd"
        val expectedFormat = "dd MMM yyyy"
        val expectedFormattedDate = "21 Jun 2023"

        val formattedDate = Helper.dateFormatter(currentDate, currentFormat, expectedFormat)

        assertEquals(expectedFormattedDate, formattedDate)
    }


}
